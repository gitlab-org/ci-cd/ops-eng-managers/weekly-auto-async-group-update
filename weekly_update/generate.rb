return unless ENV['GENERATE_WEEKLY_UPDATE'] || ENV['CI_JOB_STAGE'] == 'test'

require 'date'
require 'delegate'
require 'erb'
require 'gitlab'
require 'httparty'
require 'json'
require 'nokogiri'
require 'optparse'
require 'optparse/date'
require 'set'
require 'yaml'

require_relative 'capacity'
require_relative 'contribution'
require_relative 'description'
require_relative 'error_budget'
require_relative 'issue'
require_relative 'issue_distribution'
require_relative 'janky_cache'
require_relative 'markdown'
require_relative 'milestone_plan'
require_relative 'mr'
require_relative 'team'
require_relative 'team_activity'
require_relative 'weekly_update'

# Generates a summary issue of group activities occurred during
# the current or specified week.
class Generator
  attr_reader :config, :today, :options, :gitlab_api

  def initialize(config, options)
    @config = config
    @today = options[:date] || Date.today
    @options = options
  end

  def run(api_token)

    team = Team.new(config)
    puts "Running weekly update for #{today.to_s} - #{today.year}W#{today.cweek} for #{team.display_name}"

    connect(api_token)
    generate(team)

  end

  def connect(api_token)
    puts 'Connecting to GitLab...'

    @gitlab_api = Gitlab.client(
      endpoint: 'https://gitlab.com/api/v4',
      private_token: api_token
    )

    puts "Connection successful - API: authenticated as #{@gitlab_api.user.username}"
  end

  def generate(team)
    current_issue = WeeklyUpdate.new(gitlab_api, today, team)
    description_builder_current = Description.new(team, options[:skip], current_issue)

    find_or_create(description_builder_current, current_issue)
    update(description_builder_current, current_issue)

    add_comments(current_issue)
  end

  def find_or_create(description_builder, update_issue)
    if update_issue.current.nil?
      current = update_issue.create!(description_builder, dry_run: options[:"dry-run"])
      log_issue(current, 'Created')
    else
      log_issue(update_issue.current, 'Found')
    end
  end
  def update(description_builder, update_issue)
    issue = update_issue.update!(description_builder, dry_run: options[:"dry-run"])
    log_issue(issue, 'Updated')
  end

  def add_comments(update_issue)
    comments = update_issue.comment!(dry_run: options[:"dry-run"])
    comments.map { log_comment(_1) }
  end

  def log_issue(issue, prefix)
    puts "\n\n\n\n"
    puts "#{prefix} issue #{issue.title}: ##{issue.id} - #{issue.web_url}"
    puts "\n\n"
    puts issue.description
    puts "\n\n\n\n"
  end

  def log_comment(comment)
    puts "\n\n\n\n"
    puts "Threads added for issue:\n"
    puts comment.notes.first.body
    puts "\n\n\n\n"
  end
end

if $PROGRAM_NAME == __FILE__
  begin
    api_token = ENV['GITLAB_API_PRIVATE_TOKEN']
    options = { skip: [] }

    OptionParser.new do |opts|
      opts.banner = "Usage: generate.rb [options]"

      opts.on("--dry-run", "Dry run")
      opts.on("-s", "--skip [SECTION]", Array, "Skip sections")
      opts.on("-d", "--date [DATE]", Date, "Date to use as run date")

    end.parse!(into:options)

    options[:skip].map! { |o| o.gsub('_', '-').to_sym  }

    Dir.glob("config/*.yml") do |config_file|
      config = YAML.load(ERB.new(File.read(config_file)).result)
      JankyCache.reset
      Generator.new(config, options).run(api_token)
    end
    
  rescue StandardError => e
    puts "#{e.class}: #{e.message}"
    puts e.backtrace
    raise
  end
end