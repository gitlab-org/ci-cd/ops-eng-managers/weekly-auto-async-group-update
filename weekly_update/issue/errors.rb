module IssueErrors
  def errors
    messages = []

    check_issue_types(messages)
    check_merge_request_milestones(messages)
    check_workflow_label(messages)
    check_domain_labels(messages)

    messages
  end

  private

  def check_issue_types(messages)
    unless type
      messages << "check for missing type label"
    end

    unless sub_type
      messages << "check for missing subtype label"
    end
  end

  def check_merge_request_milestones(messages)
    return if security?

    open_mrs.select { _1.milestone&.expired }.each do |mr|
      messages << "check MR milestone for accuracy - #{mr.milestone.title} is expired. #{mr.web_url}+"
    end
  end

  def check_stale_merge_requests
    pending_mrs.each do |mr|
      messages << "check if MR is stalled - MR is open & green but is not in review. #{mr.web_url}+"
    end
  end

  def check_workflow_label(messages)
    label = relevant_labels.find { |label| label.include?("workflow::") }

    unless label
      messages << "check workflow label for accuracy - workflow label is absent"
    end

    if label == "workflow::planning breakdown"
      messages << "check workflow label for accuracy - planning should generally be completed before scheduling implementation"
    end

    if draft_mrs.none? && (open_mrs & directly_linked_mrs).any?
      if ["workflow::ready for development", "workflow::in dev"].include?(label)
        messages << "check workflow label for accuracy - actual is ~\"#{label}\", but there are open MRs"
      end
    end

    if team_assignees.any? || (assignees.any? && team_assignee_optional?)
      if label == "workflow::ready for development"
        messages << "check workflow label for accuracy - actual is ~\"#{label}~\", but there is an assignee"
      end
    end

    if awaiting_review.any? && label == "workflow::blocked"
      messages << "check workflow label for accuracy - this has MRs awaiting review"
    end
  end

  def check_domain_labels(messages)
    if (%w(backend frontend) & domains).empty? && !team_assignee_optional?
      return if domains.include?('documentation') && title.include?('document')

      messages << "check for missing domain labels - both ~backend and ~frontend are absent"
    end
  end
end