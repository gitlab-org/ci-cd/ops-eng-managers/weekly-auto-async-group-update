module IssueMergeRequests
    ISSUE_URL_REGEX = /https:\/\/gitlab.com\/(?<project>.+)\/-\/issues\/(?<iid>\d+)/
    RELATED_ISSUE_REGEX = '\b((?:[Cc]los(?:e[sd]?|ing)|[rR]elated to|[pP]art of|[Cc]ontribut(?:e|es|[ed]?|ing) to|\b[Ff]ix(?:e[sd]|ing)?|\b[Rr]esolv(?:e[sd]?|ing)|\b[Ii]mplement(?:s|ed|ing)?)(:?)\s+(?:(?:issues?\s+)?(\S+%{issue_ref})(?:(?: *,? +and +| *,? *)?)|([A-Z][A-Z0-9_]+-\d+))+)'

  attr_accessor :merge_requests

  def review_status(weekly_update)
    @review_status ||= [
      ("Relevant MRs: (#{relevant_mrs.size})" if relevant_mrs.any?),
      ("Draft MRs (#{draft_mrs.size}): #{wrap_mrs(draft_mrs.map(&:web_url))}" if draft_mrs.any?),
      ("In review MRs (#{awaiting_review.size}): #{wrap_mrs(awaiting_review.map(&:web_url))}" if awaiting_review.any?),
      ("Reviewed/stalled MRs (#{pending_mrs.size}): #{wrap_mrs(pending_mrs.map(&:web_url))}" if pending_mrs.any?),
      ("Merged MRs (#{merged_mrs.size}): #{wrap_mrs(merged_mrs.map(&:web_url))}" if merged_mrs.any?),
      ("Dependent MRs (#{chained_mrs.size}): #{wrap_mrs(chained_mrs.map(&:web_url))}" if chained_mrs.any?)
    ].compact
  end

  def all_relevant_mrs_open?
    draft_mrs.none? && (open_mrs & directly_linked_mrs).any?
  end

  def relevant_mrs
    @relevant_mrs ||= begin
      all_mrs = merge_requests.select do |mr|
        team.all.include?(mr.assignee&.username) ||
          team.all.include?(mr.author.username)
      end
      # binding.pry
      (all_mrs - mrs_linked_to_other_issues + directly_linked_mrs).uniq(&:id)
    end
  end

  def draft_mrs
    @draft_mrs ||= open_mrs.select(&:draft)
  end

  def awaiting_review
    @awaiting_review ||= open_mrs.select { |mr| mr.reviewers.any? }
  end

  def open_mrs
    @open_mrs ||= relevant_mrs.select { |mr| mr.state == "opened" }
  end

  def directly_linked_mrs
    @directly_linked_mrs ||= merge_requests.select do |mr|
      mr.description.match?(RELATED_ISSUE_REGEX.sub('%{issue_ref}', iid.to_s)) ||
      mr.title.include?(title)
    end
  end

  def pending_mrs
    @pending_mrs ||= open_mrs.select { |mr| mr.reviewers.none? && !mr.draft && mr.target_branch == 'master' }
  end

  private

  def mrs_linked_to_other_issues
    @mrs_linked_to_other_issues ||= merge_requests.select do |mr|
      data = mr.description.match(RELATED_ISSUE_REGEX.sub('%{issue_ref}', ".{0, 100}\b\d{0,20}\b"))
      data && !data[0].include?(iid.to_s)
    end
  end

  def merged_mrs
    @merged_mrs ||= relevant_mrs.select { |mr| mr.state == "merged" }
  end

  def chained_mrs
    @chained_mrs ||= directly_linked_mrs.select { |mr| mr.state == "opened" && mr.target_branch != "master" }
  end

  def wrap_mrs(mrs)
    mrs.each_slice(2).map { _1.join(", ") }.join("<br/>")
  end


end