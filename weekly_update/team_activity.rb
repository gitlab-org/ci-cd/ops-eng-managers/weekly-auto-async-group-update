# Aggregates team contributions starting from last week Friday to this week Friday
class TeamActivity
  attr_reader :weekly_update, :gitlab_api, :date, :team

  PAGE_LIMIT = 50
  IGNORED_ACTIONS = [
    "deleted branch",
    "pushed to branch",
    "pushed new branch",
    "joined project",
    "pushed new tag"
  ]

  def initialize(weekly_update, date, team)
    @weekly_update = weekly_update
    @gitlab_api = weekly_update.gitlab_api
    @date = date
    @team = team
  end

  def contributions
    return @contributions if @contributions

    @contributions = activity.map { |event| Contribution.new(event, team) }
    collect_contribution_detail
    @contributions
  end

  def start_date
    @start_date ||= Date.commercial(date.year, (date.cweek - 1), 5)
  end

  def end_date
    @end_date ||= Date.commercial(date.year, date.cweek, 6)
  end

  private

  def activity
    puts "Collecting activity for date range: #{format_date(start_date)} - #{format_date(end_date)}"

    team.all.flat_map do |user|
      puts "Collecting activity for #{user}."

      # Start events as nil rather an empty array in case of no contributions
      events = nil
      oldest_date = nil
      page = 0

      while events.nil? || oldest_date >= start_date
        puts "--- Page #{page}, starting from #{oldest_date || date}"

        oldest_date ||= end_date
        events ||= []

        in_range_events = collect_activity_for_user(user, page)
        events.concat(in_range_events.select { |event| (start_date..end_date).cover?(event[:date]) })

        oldest_date = in_range_events.any? ? in_range_events.last[:date] : start_date.prev_day
        page += 1
      end

      events
    end
  end

  def collect_activity_for_user(user, page)
    user_contribution_html(user, page).css(".event-item").filter_map do |event|
      next unless (type = parse_event_type(event))

      {
        type: type,
        url: parse_event_url(event),
        date: parse_event_date(event),
        user: user
      }
    end
  end

  def user_contribution_html(user, page)
    offset = page * PAGE_LIMIT
    path = "https://gitlab.com/users/#{user}/activity.json?page=#{page}&offset=#{offset}&limit=#{PAGE_LIMIT}"
    output = HTTParty.get(path)

    Nokogiri::HTML(output['html'])
  end

  def parse_event_type(event)
    type = event.at_css('.event-type')&.content&.gsub("\n", "")

    return unless type # private contribution
    return if IGNORED_ACTIONS.include?(type)

    type == "commits pushed new branch" ? "new branches pushed" : type
  end

  def parse_event_url(event)
    event.at_css('.event-title a').attr('href').split('#').first
  end

  def parse_event_date(event)
    date = event.at_css('.event-item-timestamp time').attr('datetime')

    Date.parse(date)
  end

  def collect_contribution_detail
    contributions.each do |contribution|
      if (details = JankyCache.read(contribution.url))
        contribution.details = details
      end
    end

    contributions.reject(&:details).group_by(&:api_path).each do |path, all_events|
      next unless path
      event = all_events.first

      case event.object
      when *Contribution::BATCHABLE_OBJECTS
        batch_fetch(path, event, all_events)
      when *Contribution::NON_LABELED_OBJECTS
        # skip fetching details if object can't have labels
      else
        fetch(path, event, all_events)
      end
    end
  end

  def fetch(path, event, all_events)
    puts "Fetching #{event.object} detail for iid #{event.iid} in project #{event.project}..."
    details = JankyCache.fetch(path) { gitlab_api.get(path) }
    all_events.each { |e| e.details = details }
  end

  def batch_fetch(path, event, all_events)
    all_events.map(&:iid).uniq.each_slice(100).each do |iids|
      puts "Fetching #{event.object} detail for iids #{iids} in project #{event.project}..."

      gitlab_api
        .get(path + '.json' + "?iids[]=" + iids.join('&iids[]=') + '&per_page=100')
        .each { |details| match_contributions_to_detail(all_events, details) }
    end
  end

  def match_contributions_to_detail(cons, details)
    matching_cons = cons.select do |contribution|
      contribution.iid.to_i == details.iid.to_i
    end

    JankyCache.write(matching_cons.first.url, details)

    matching_cons.each do |contribution|
      contribution.details = details
    end
  end

  def format_date(timestamp)
    timestamp.strftime("%a %Y-%m-%d")
  end
end