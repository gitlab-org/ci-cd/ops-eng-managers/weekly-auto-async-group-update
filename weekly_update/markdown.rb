# Formatting helper for generating issue description GitLab-flavored markdown
class Markdown
  class << self
    # @ params header Array<String>
    # @ params header Array<Array<String>>
    def table(header, rows)
      divider = [" ----- "] * header.length
      all_rows = [header, divider, *rows].map { wrap_table_row(_1) }

      all_rows.join("\n")
    end

    def multiline_cell(lines, compact: true, divider: false, bulleted: false)
      lines = lines.compact
      return if lines.none?

      lines = lines.map { "- #{_1}" } if bulleted
      separater = "<br/> "
      separater += "<hr/>" if divider

      lines.join(separater)
    end

    def link(text, url)
      "[#{text}](#{url})"
    end

    def gitlab_link(path, obfuscated: false)
      prefix = 'https://gitlab.com' unless path.include?("http")
      suffix = '+' if path.match(/(merge_requests|issues|epics)\/\d+$/)

      "#{prefix}#{path}#{suffix}"
    end

    def list(items, compact: true)
      items = items.compact if compact
      items.join(", ")
    end

    def labelify(label)
      "~\"#{label}\"" if label
    end

    def label_list(labels)
      list = labels.map { labelify(_1) }
      list.join(" ") if list.any?
    end

    def userify(username)
      "`@#{username}`" if username
    end

    def user_list(usernames)
      list = usernames.map { userify(_1) }
      list.join(" ") if list.any?
    end

    def percentage(numerator, denominator = 1, precision: 0)
      return unless denominator != 0

      "#{(numerator.fdiv(denominator) * 100).round(precision)}%"
    end

    def issues_as_markdown_list(issues)
      lines = []

      issues.each do |issue|
        lines << "  * [#{issue.title}](#{issue.web_url}) - <Update>"
      end

      lines.join("\n")
    end

    private

    def wrap_table_row(row)
      "| #{row.join(" | ")} |"
    end

  end
end
