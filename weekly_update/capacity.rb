# Queries a Google App Script which exposes aggregate team PTO from Google Calendar
class Capacity
  attr_reader :team, :milestone

  def initialize(team, start_date, end_date)
    @team = team
    @start_date = start_date
    @end_date = end_date
    @pto_app_token = ENV['PTO_APP_TOKEN']
  end

  def response
    @response ||= response = HTTParty.get(capacity_url)
  end

  # Use next week to get a forward-looking capacity
  def capacity_url
    team.capacity_url + "?method=capacity&pat=#{@pto_app_token}&start_date=#{@start_date}&due_date=#{@end_date}"
  end

  def days_in_period
    response['milestone_days'] || 0
  end

  def frontend_devs
    response['frontend_devs'] || 0
  end

  def backend_devs
    response['backend_devs'] || 0
  end

  def frontend_pto
    response['frontend_pto'] || 0
  end

  def backend_pto
    response['backend_pto'] || 0
  end

  def frontend_days
    response['frontend_days_allocated'] || 0
  end

  def backend_days
    response['backend_days_allocated'] || 0
  end

  def full_capacity_frontend_days
    response['frontend_days_full'] || 0
  end

  def full_capacity_backend_days
    response['backend_days_full'] || 0
  end

  def engineer_days
    frontend_days + backend_days
  end

  def full_capacity_engineer_days
    full_capacity_frontend_days + full_capacity_backend_days
  end

  def frontend_deficit
    frontend_pto + frontend_borrow
  end

  def backend_deficit
    backend_pto + backend_borrow
  end

  def frontend_borrow
    full_capacity_frontend_days - frontend_days
  end

  def backend_borrow
    full_capacity_backend_days - backend_days
  end

  def team_deficit
    backend_deficit + frontend_deficit
  end

  def team_total
    frontend + backend
  end

  def frontend
    frontend_days - frontend_pto
  end

  def backend
    backend_days - backend_pto
  end

end