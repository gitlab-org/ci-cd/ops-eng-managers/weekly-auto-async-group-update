# frozen_string_literal: true
require_relative './metrics/results'

module Metrics
  Group = Struct.new(:name, :stage)

  def self.load!(bucket, date, file_regex, metric_types)
    file = bucket.files(prefix: date.strftime('%Y%m%d'))
                 .find { |f| f.name.end_with?(file_regex) }
    raise "No error budget file found for #{date}" unless file

    content = JSON.parse(file.download.read)

    results = {}

    metric_types.each do |metric_type|
      raw_results = content.fetch(metric_type, nil)
      raise "Invalid metric content for #{metric_type}" unless raw_results

      metrics = raw_results.dig('body', 'data', 'result')
      raise "Missing metrics for #{metric_type}" unless metrics

      results[metric_type] = Results.new(metrics)
    end

    results
  end
end
