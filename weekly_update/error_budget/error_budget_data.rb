#!/usr/bin/env ruby
# frozen_string_literal: true

require 'google/cloud/storage'

require_relative './metrics'
require_relative './team_error_budget'

def load_error_budget_data(gcp_project, gcs_bucket, gcp_keyfile, team_name, report_date, seven_day)

  date = report_date - 1
  file_regex = seven_day ? 'stage-group-7d-error-budgets.json' : 'stage-group-error-budgets.json'

  storage = Google::Cloud::Storage.new(project_id: gcp_project, credentials: gcp_keyfile)
  # Skip the lookup, so the key does not need the `storage.buckets.list` permission
  bucket = storage.bucket(gcs_bucket, skip_lookup: true)

  required_metrics = %w[stage_group_error_budget_availability
                      stage_group_error_budget_seconds_spent
                      stage_group_error_budget_seconds_remaining]
  metrics = Metrics.load!(bucket, date, file_regex, required_metrics)
  metrics_availability, metrics_sec_spent, metrics_sec_remaining = metrics.values_at(*required_metrics)

  team_error_budget = TeamErrorBudget.new(team_name)
  team_error_budget.add_availability(metrics_availability)
  team_error_budget.add_seconds_spent(metrics_sec_spent)
  team_error_budget.add_seconds_remaining(metrics_sec_remaining)

  team_error_budget

end

