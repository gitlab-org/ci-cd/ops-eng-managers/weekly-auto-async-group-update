# frozen_string_literal: true

class TeamErrorBudget
  attr_reader :name, :channels, :availability, :seconds_spent, :seconds_remaining, :timestamp

  def initialize(team_name)
    @name = team_name
  end

  def add_availability(metrics)
    @timestamp, @availability = get_metric_for_group(metrics)
  end

  def add_seconds_spent(metrics)
    _timestamp, @seconds_spent = get_metric_for_group(metrics)
  end

  def add_seconds_remaining(metrics)
    _timestamp, @seconds_remaining = get_metric_for_group(metrics)
  end

  def stage_group?
    !name.nil?
  end

  def metrics?
    !availability.nil? && !seconds_spent.nil? && !seconds_remaining.nil?
  end

  private

  def get_metric_for_group(metrics)
    return nil unless stage_group?

    matching_metric = metrics.select { |metric| metric["metric"]["stage_group"] == @name }
    return nil unless matching_metric.last

    # This is a tuple of `timestamp, value`
    matching_metric.last["value"]
  end
end
