# frozen_string_literal: true

module Metrics
  class Results
    include Enumerable

    def initialize(metrics)
      @metrics = metrics.sort_by { |metric| metric["value"].last.to_f }
    end

    def each(&block)
      @metrics.each(&block)
    end

    def by_group
      @by_group ||= @metrics.each_with_object({}) do |metric, result|
        group = Metrics::Group.new(metric['metric']['stage_group'], metric['metric']['product_stage'])
        result[group] = metric["value"].last
      end
    end
  end
end
