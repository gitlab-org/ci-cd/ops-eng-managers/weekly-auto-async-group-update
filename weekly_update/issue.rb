require_relative 'issue/completion'
require_relative 'issue/errors'
require_relative 'issue/merge_requests'

# Represents an issue the team is working on.
# Responsible for estimating status & completion of the tasks.
# See MilestonePlan for details on initialization.
class Issue < SimpleDelegator
  include IssueErrors
  include IssueMergeRequests
  include IssueCompletion
  attr_reader :team

  TYPE_LABELS = ["type::", "bug::", "feature::", "maintenance::", "workflow::"]
  DOMAIN_LABELS = ["backend", "frontend", "documentation", "UX", "database", "Quality"]
  NOTABLE_LABELS = ["severity::", "backend complete", "bug::vulnerability", "deprecation", "removal", "Community contribution", "SLO::", "Deliverable"]

  def initialize(obj, team)
    super(obj)
    @team = team
  end
  def type
    @type ||= relevant_labels.find { |label| label.include?("type::") }
  end

  def sub_type
    @sub_type ||= relevant_labels.find do |label|
      ["bug::", "feature::", "maintenance::"].any? { |prefix| label.include?(prefix) }
    end
  end

  def planning?
    type == 'type::ignore'
  end

  def epic_title
    @epic_title ||= epic&.title
  end

  def status
    @status ||= begin
      label = relevant_labels.find { |label| label.include?("workflow::") }

      if label == 'workflow::ready for development'
        if assigned? || directly_linked_mrs.any?
          label = 'workflow::in dev'
        end
      end

      if label == 'workflow::in dev' && all_relevant_mrs_open?
        label = 'workflow::in review'
      end

      if label.nil?
        label = 'workflow::ready for development'
      end

        "~\"#{label}\""
    end
  end

  def weight
    @weight ||= begin
      domains_with_work = domains.dup
      domains_with_work.delete('backend') if labels.include?("backend complete")
      domains_with_work.push('security') if security?

      [1, domains_with_work.length].max
    end
  end

  def domains
    @domains ||= (DOMAIN_LABELS & relevant_labels) | (DOMAIN_LABELS & relevant_mrs.flat_map(&:labels))
  end

  def engineering_domains
    @engineering_domains ||= (domains & ['backend', 'frontend']).sort
  end

  def notable_labels
    @notable_labels ||= (NOTABLE_LABELS & relevant_labels) | (NOTABLE_LABELS & relevant_mrs.flat_map(&:labels))
  end

  def missing_assignee
    @missing_assignee ||= if !assignee
      'No assignee'
    elsif team_assignees.empty?
      "No ~\"#{team.group_label}\" assignee" unless team_assignee_optional?
    elsif domains.include?('backend') && (team_assignees & (team.backend + team.fullstack)).empty?
      'No ~backend assignee'
    elsif domains.include?('frontend') && (team_assignees & (team.frontend + team.fullstack)).empty?
      'No ~frontend assignee'
    end
  end

  def past_due?
    labels.include?("SLO::Missed")
  end

  def deliverable?
    labels.include?("Deliverable")
  end

  def closed?
    state == 'closed'
  end

  private

  def relevant_labels
    @relevant_labels ||= labels.select do |label|
      DOMAIN_LABELS.any? { |full_label| label == full_label } ||
      (TYPE_LABELS + NOTABLE_LABELS).any? { |prefix| label.include?(prefix) }
    end
  end

  def assigned?
    assignees.any? && team_assignee_optional? || team_assignees.any?
  end

  def team_assignees
    @team_assignees ||= assignees.map(&:username) & team.all
  end

  def team_assignee_optional?
    community_contribution? || planning? || deprecation? || qa?
  end

  def community_contribution?
    labels.include?("Community contribution")
  end

  def deprecation?
    labels.include?("deprecation")
  end

  def qa?
    domains.include?("Quality")
  end

  def security?
    sub_type == "bug::vulnerability"
  end
end
