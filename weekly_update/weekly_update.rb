# Represents a weekly update issue
class WeeklyUpdate
  attr_reader :gitlab_api, :date, :project_id, :team

  def initialize(gitlab_api, date, team)
    @gitlab_api = gitlab_api
    @team = team
    @project_id = team.update_project_id
    @date = date
    @threads = []
  end

  #TODO: This has issues around the end of year. Fix properly.
  def previous
    @previous ||= find_issue(date.cweek - 1, date.year)
  end

  def current
    @current ||= find_issue(date.cweek, date.year)
  end

  def upcoming
    @upcoming ||= find_issue(date.cweek + 1, date.year)
  end

  def plan
    @plan ||= MilestonePlan.new(gitlab_api, date, team, self)
  end

  def create!(description_builder, dry_run: false)
    return mock_issue(:CREATE, description_builder) if dry_run

    body = description_builder.render(description, :CREATE)
    gitlab_api.create_issue(
      project_id,
      title,
      { description: body }
    ).tap { JankyCache.write("update-issue-#{date.cweek}", _1) }
  end

  def update!(description_builder, dry_run: false)
    return mock_issue(:UPDATE, description_builder) if dry_run

    body = description_builder.render(description, :UPDATE)
    gitlab_api.edit_issue(
      project_id,
      current.iid,
      { description: body }
    )
  end

  def comment!(dry_run: false)
    @threads.filter_map do |comment|
      next mock_comment(comment) if dry_run

      # Discussions API isn't supported with gitlab client
      gitlab_api.post(
        "/projects/#{project_id}/issues/#{current.iid}/discussions",
         body: { body: comment }
      )
    end
  end

  def add_thread(thread)
    @threads << thread
  end

  # Reference point for "start date" of the current week.
  # Used from capacity, team activity, etc
  def monday
    Date.commercial(date.year, date.cweek, 1)
  end

  def friday
    Date.commercial(date.year, date.cweek, 5)
  end

  def update_time
    current_time = Time.new
    current_time.iso8601
  end
  private

  def title
    "Draft: #{team.display_name} Engineering Async Updates - #{date.year}W#{date.cweek} - ending on #{friday.strftime("%b-%d-%Y")}"
  end

  def description
    current&.description || template
  end

  def find_issue(week, year)
    JankyCache.fetch("update-issue-#{week}") {
      gitlab_api.issues(
        project_id,
        labels: ["OpsSection::Weekly-Update", team.group_label],
        search: "#{year}W#{week}",
        in: "title"
      ).first
    }
  end

  def template
    File.read(".gitlab/issue_templates/#{team.update_template}")
  end

  def mock_issue(action, description_builder)
    body = description_builder.render(description, action)
    MockIssue.new(-1, project_id, title, body, "https://mock.issue/-1")
  end

  def mock_comment(comment)
    MockDiscussion.new(
      -1,
      [ MockComment.new(comment) ]
    )
  end

  # Mock API response objects for dry-run
  MockComment = Struct.new(:body)
  MockDiscussion = Struct.new(:id, :notes)
  MockIssue = Struct.new(:iid, :project_id, :title, :description, :web_url) do
    alias_method :id, :iid

    def web_url
      "/projects/#{project_id}/issues/#{iid}"
    end
  end
end
