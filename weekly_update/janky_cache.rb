class JankyCache
  def self.janky_cache
    @@cache ||= {}
  end

  def self.fetch(key)
    read(key) || write(key, yield(key))
  end

  def self.write(key, value)
    janky_cache[key] = value
  end

  def self.read(key)
    janky_cache[key]
  end

  def self.reset
    @@cache = {}
  end
end