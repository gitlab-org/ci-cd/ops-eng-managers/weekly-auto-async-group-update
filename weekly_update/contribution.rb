# Represents an event under the activity tab on a user profile
# See TeamActivity for details on initialization.
class Contribution
  EXPECTED_OBJECTS = %w(issues merge_requests commits work_items epics)
  OBJECT_URL_REGEX = /https:\/\/gitlab.com\/(groups\/)?(?<project>.+)\/-\/(?<object>#{EXPECTED_OBJECTS.join('|')})\/(?<iid>\d+)/
  BATCHABLE_OBJECTS = %w(issues merge_requests work_items)
  NON_LABELED_OBJECTS = %w(branches commits)

  attr_reader :url, :project, :iid, :team
  attr_reader :action, :path, :date, :username
  attr_accessor :details

  def initialize(event, team)
    @event = event
    @action, @path, @date, @username = event.values

    prefix = 'https://gitlab.com' unless path.include?("http")
    @url = "#{prefix}#{path}"
    @project, @object, @iid = OBJECT_URL_REGEX.match(url)&.captures
    @team = team
  end

  def object
    return "branches" if @object == "commits"

    @object
  end

  # Used to retrieve group details from labels on object
  def api_path
    return unless (EXPECTED_OBJECTS - NON_LABELED_OBJECTS).include?(object)
    return "/groups/#{CGI.escape(project)}/#{object}/#{iid}" if object == "epics"
    return "/projects/#{CGI.escape(project)}/issues" if object == "work_items"

    "/projects/#{CGI.escape(project)}/#{object}"
  end

  # Identify which area contribution is going toward, but many
  # objects/projects are missing identifying labels
  def primary_label
    return @label if @label
    return unless details

    @label ||= group
    @label ||= match_label("devops::")
    @label ||= match_label("section::")
    @label ||= category
    @label ||= match_label("Engineering Productivity")
    @label ||= match_label("Community contribution")
    @label ||= match_label("security")
    @label ||= match_label("documentation")
    @label ||= match_label("maintainer application")
    @label ||= match_label("maintenance::dependency")
    @label ||= match_label("automation:bot-authored")
    @label ||= match_label("component::")
    @label ||= team.group_label if project == team.planning_issue_project
    @label
  end

  def type
    @type ||= match_label("type::")
  end

  def group
    @group ||= match_label("group::")
  end

  def category
    @category ||= match_label("Category:")
  end

  private

  def match_label(substring)
    return unless details

    details.labels.find { |label| label.include?(substring) }
  end
end