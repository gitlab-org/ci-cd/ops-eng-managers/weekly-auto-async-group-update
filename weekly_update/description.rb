require_relative 'fiscal_calendar'
class Description
  attr_reader :sections, :team, :weekly_update, :plan
  include FiscalCalendar

  SECTIONS = {
    'navigation': :navigation_markdown,
    'planning-issue': :planning_issues_markdown,
    'capacity': :week_capacity_markdown,
    'milestone-capacity': :milestone_capacity_markdown,
    'error-budget': :error_budget_markdown,
    'infradev-issues': :infradev_issues_markdown,
    'security-issues': :security_issues_markdown,
    'corrective-action-issues': :corrective_action_issues_markdown,
    'request-for-help-issues': :help_issues_markdown,
    'deliverables-breakdown': :deliverables_breakdown_markdown,
    'all-issues-breakdown': :all_issue_breakdown_markdown,
    'okr-table': :okr_table_markdown,
    'mr-breakdown': :mr_breakdown_markdown,
    'community-contributions': :community_contributions_markdown,
    'issue-distribution': :issue_distribution_markdown,
    'progress-summary': :progress_summary_markdown,
    'progress-detail': :progress_detail_markdown,
    'team-activity': :team_activities_markdown,
    'rollover': :rollover_issues_markdown,
    'team-links': :team_links_markdown,
    'quick-actions': :quick_actions_markdown,
    'highlights': :highlights_markdown
  }

  def initialize(team, skipped_sections, weekly_update)
    raise "Invalid section in #{skipped_sections}" unless (skipped_sections - SECTIONS.keys).length == 0

    @team = team
    @sections = SECTIONS.reject { skipped_sections.include?(_1) }
    @weekly_update = weekly_update
    @plan = weekly_update.plan
  end

  def render(template_markdown, action)
    unwrap_threads(template_markdown) if action == :UPDATE
    sections.each do |section, method|
      upcase_section = section.to_s.upcase
      puts "\nStarting generation for #{upcase_section} section...\n"
      next unless template_markdown.match?(/<!-- REPLACE-WITH-#{upcase_section}(-END|-START)?:#{action} -->/)

      result = send(method).to_s

      while (range = get_block_range(template_markdown, upcase_section, action))
        template_markdown[range] = result
      end
    end

    wrap_threads(template_markdown) if action == :CREATE
    collect_threads!(template_markdown, action)

    template_markdown
  end

  def get_block_range(markdown, section, action)
    oneliner = "<!-- REPLACE-WITH-#{section}:#{action} -->"

    if (i = markdown.index(oneliner))
      return (i...(i + oneliner.length))
    end

    start_text = "<!-- REPLACE-WITH-#{section}-START:#{action} -->"
    end_text = "<!-- REPLACE-WITH-#{section}-END:#{action} -->"

    return unless (j = markdown.index(start_text))
    return unless (k = markdown.index(end_text))

    (j...(k + end_text.length))
  end

  def get_content_range(markdown, section, action)
    start_text = "<!-- REPLACE-WITH-#{section}-START:#{action} -->"
    end_text = "<!-- REPLACE-WITH-#{section}-END:#{action} -->"

    return unless j = markdown.index(start_text)
    return unless k = markdown.index(end_text)

    ((j + start_text.length)...k)
  end

  def collect_threads!(markdown, action)
    while range = get_block_range(markdown, :THREAD, action)
      comment_range = get_content_range(markdown[range], :THREAD, action)

      weekly_update.add_thread(markdown[range][comment_range])

      markdown[range] = ''
    end
  end

  def wrap_threads(markdown)
    while range = get_block_range(markdown, :THREAD, :UPDATE)
      comment_range = get_content_range(markdown[range], :THREAD, :UPDATE)
      comment = markdown[range][comment_range]

      comment = comment.gsub('"', '&quot;')
      comment = comment.split("\n").join("\\\n")

      comment = <<~MARKDOWN
        <!-- REPLACE-WITH-HIDDEN-START:UPDATE -->
        <div title=\"\\\n#{comment}\n\"\/>
        <!-- REPLACE-WITH-HIDDEN-END:UPDATE -->
      MARKDOWN

      markdown[range] = comment
    end
  end

  def unwrap_threads(markdown)
    while range = get_block_range(markdown, :HIDDEN, :UPDATE)
      comment_range = get_content_range(markdown[range], :HIDDEN, :UPDATE)
      comment = markdown[range][comment_range]

      comment = comment.gsub(/^<div title=\"\\\n/, '')
      comment = comment.gsub(/\n\"\/>$/, '')
      comment = comment.split("\\\n").join("\n")
      comment = comment.gsub('&quot;', '"')

      comment = <<~MARKDOWN
        <!-- REPLACE-WITH-THREAD-START:UPDATE -->
        #{comment}
        <!-- REPLACE-WITH-THREAD-END:UPDATE -->
      MARKDOWN

      markdown[range] = comment
    end
  end

  def navigation_markdown
    nav_items = []

    if weekly_update.previous
      nav_items << "[< previous update](#{weekly_update.previous.web_url})"
    end

    if weekly_update.upcoming
      nav_items << "[next update >](#{weekly_update.upcoming.web_url})"
    end

    return unless nav_items.any?

    divider = ['---'] * nav_items.length

    <<~MARKDOWN

      | #{nav_items.join(' | ')} |
      | #{divider.join(' | ')} |

    MARKDOWN
  end

  def milestone_capacity_markdown
    start_date = plan.milestone.start_date
    end_date = plan.milestone.due_date

    <<~MARKDOWN.chomp
      _For milestone #{plan.milestone.title} as of, #{weekly_update.update_time}._

      #{capacity_markdown(start_date, end_date)}
    MARKDOWN
  end

  def week_capacity_markdown

    start_date = weekly_update.monday
    end_date = start_date + 7

    <<~MARKDOWN.chomp
      _For week starting #{start_date} as of, #{weekly_update.update_time}._

      #{capacity_markdown(start_date, end_date)}
    MARKDOWN

  end
  
  def capacity_markdown(start_date, end_date)
    capacity = Capacity.new(team, start_date, end_date)
    total_percent = Markdown.percentage(capacity.team_total, capacity.full_capacity_engineer_days)
    frontend_percent = Markdown.percentage(capacity.frontend, capacity.full_capacity_frontend_days)
    backend_percent = Markdown.percentage(capacity.backend, capacity.full_capacity_backend_days)

    frontend_calculation = {
      days: capacity.full_capacity_frontend_days,
      PTO: capacity.frontend_pto,
      Borrow: capacity.frontend_borrow
    }.select { _2 > 0 }

    backend_calculation = {
      days: capacity.full_capacity_backend_days,
      PTO: capacity.backend_pto,
      Borrow: capacity.backend_borrow
    }.select { _2 > 0 }
    
    <<~MARKDOWN.chomp
      <details><summary>Capacity: #{total_percent} (#{frontend_percent} of ~frontend, #{backend_percent} of ~backend)</summary>

        - ~frontend: max of #{capacity.frontend_devs} engineers, #{capacity.days_in_period} weekdays
          - $`#{capacity.frontend}(days) = #{frontend_calculation.map { "#{_2}(#{_1})" }.join(' - ')}`$
        - ~backend: max of #{team.backend.length} engineers, #{capacity.days_in_period} weekdays
          - $`#{capacity.backend}(days) = #{backend_calculation.map { "#{_2}(#{_1})" }.join(' - ')}`$

        _Data from: [Shared Calendar](#{team.team_calendar}),
        via [Google App Scripts](#{team.capacity_url})_

        ---

      </details>
    MARKDOWN
  end


  def error_budget_markdown
    if weekly_update.date <= Date.today

      budget = ErrorBudget.new(weekly_update.date, team)
      percentage_28d = Markdown.percentage(budget.availability_28d, precision: 2) if budget.availability_28d
      percentage_7d = Markdown.percentage(budget.availability_7d, precision: 2) if budget.availability_7d

      start_time = Time.now

      from_28d_time = ((start_time).to_i - (28*24*60*60)) * 1000
      from_7d_time = ((start_time).to_i - (7*24*60*60)) * 1000
      to_time = start_time.to_i * 1000

      <<~MARKDOWN.chomp
        
        <details><summary>Error budget: #{budget.status_28d} #{budget.availability_28d ? percentage_28d : 'unknown'}</summary>
  
          <!-- If not 🟢, add detail (related issues, ongoing investigations, remediations, etc). -->
  
          _Current Data: [#{team.display_name} error budget dashboard](https://dashboards.gitlab.net/d/stage-groups-detail-#{team.group_name_snake}/stage-groups-#{team.group_name_kabob}-group-error-budget-detail?orgId=1)_
  
          ---
  
          - Last week's [28d service availability](https://dashboards.gitlab.net/d/stage-groups-detail-#{team.group_name_snake}/stage-groups-#{team.group_name_kabob}-group-error-budget-detail?orgId=1&from=#{from_28d_time}&to=#{to_time}) was #{percentage_28d} - #{budget.status_28d} 
             - #{budget.minutes_remaining_28d} minutes remaining and #{budget.minutes_spent_28d} minutes spent.
          - Last week's [7d service availability](https://dashboards.gitlab.net/d/stage-groups-detail-#{team.group_name_snake}/stage-groups-#{team.group_name_kabob}-error-budget-detail?orgId=1&from=#{from_7d_time}&to=#{to_time}) was #{percentage_7d} - #{budget.status_7d} 
             - #{budget.minutes_remaining_7d} minutes remaining and #{budget.minutes_spent_7d} minutes spent.
  
        </details>
      MARKDOWN
    else
      <<~MARKDOWN.chomp
        Too early to calculate
      MARKDOWN
    end
  end

  def infradev_issues_markdown
    total_issues = plan.infradev_issues.count
    overdue_issues = plan.infradev_issues.select{ |i| i.past_due? }.count

    <<~MARKDOWN.chomp
      * :construction: Infradev ([Total: #{total_issues}](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=#{CGI.escape(team.group_label)}&label_name%5B%5D=infradev&first_page_size=20), [Past Due: #{overdue_issues}](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=#{CGI.escape(team.group_label)}&label_name%5B%5D=infradev&label_name%5B%5D=SLO%3A%3AMissed&first_page_size=20))
      #{Markdown.issues_as_markdown_list(plan.infradev_issues)}
    MARKDOWN
  end

  def security_issues_markdown
    total_issues = plan.security_issues.count
    overdue_issues = plan.security_issues.select{ |i| i.past_due? }.count

    <<~MARKDOWN.chomp
      * :closed_lock_with_key: Security ([Total: #{total_issues}](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=#{CGI.escape(team.group_label)}&label_name%5B%5D=security&label_name%5B%5D=bug%3A%3Avulnerability&first_page_size=20), [Past Due: #{overdue_issues}](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=#{CGI.escape(team.group_label)}&label_name%5B%5D=security&label_name%5B%5D=bug%3A%3Avulnerability&label_name%5B%5D=SLO%3A%3AMissed&first_page_size=20)) [Sisense Dashboard](https://app.periscopedata.com/app/gitlab/913607/Past-Due-Security-Issues-Development)
      #{Markdown.issues_as_markdown_list(plan.security_issues)}
    MARKDOWN
  end

  def corrective_action_issues_markdown
    total_issues = plan.corrective_action_issues.count
    overdue_issues = plan.corrective_action_issues.select{ |i| i.past_due? }.count

    <<~MARKDOWN.chomp
      * :pencil2:  Corrective Actions ([Total: #{total_issues}](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=#{CGI.escape(team.group_label)}&label_name%5B%5D=corrective%20action&first_page_size=20), [Past Due: #{overdue_issues}](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=#{CGI.escape(team.group_label)}&label_name%5B%5D=corrective%20action&label_name%5B%5D=SLO%3A%3AMissed&first_page_size=20))
      #{Markdown.issues_as_markdown_list(plan.corrective_action_issues)}
    MARKDOWN
  end

  def help_issues_markdown
    total_issues = plan.help_issues.count

    <<~MARKDOWN.chomp
      * :hospital: Request for Help ([Total: #{total_issues}](https://gitlab.com/gitlab-com/ops-sub-department/section-ops-request-for-help/-/issues/?label_name%5B%5D=Help%20#{CGI.escape(team.group_label)}))
      #{Markdown.issues_as_markdown_list(plan.help_issues)}
    MARKDOWN
  end

  def deliverables_breakdown_markdown

    deliverables = plan.milestone_issues.select{ |i| i.deliverable? }
    issue_breakdown_markdown(deliverables, "Deliverables", "Workflow Status of Open Deliverables", "Overall Deliverable Say/Do", "DELIVERABLES-BREAKDOWN")

  end

  def all_issue_breakdown_markdown

    issue_breakdown_markdown(plan.milestone_issues.select{ |i| i.type != "type::ignore" }, "All Issues", "Workflow Status of Open Issues", "Overall Say/Do", "ALL-ISSUES-BREAKDOWN")

  end

  def issue_breakdown_markdown(issues, stats_heading, status_heading, say_do_heading, section)
    features = issues.select{ |i| i.type == "type::feature" }
    closed_features = features.select{ |i| i.closed? }
    bugs = issues.select{ |i| i.type == "type::bug" }
    closed_bugs = bugs.select{ |i| i.closed? }
    maintenance = issues.select{ |i| i.type == "type::maintenance" }
    closed_maintenance = maintenance.select{ |i| i.closed? }

    total = issues.count
    total_closed = closed_features.count + closed_bugs.count + closed_maintenance.count

    deliverable_param = "&label_name%5B%5D=Deliverable"
    if section == "ALL-ISSUES-BREAKDOWN"
      deliverable_param = ""
    end

    <<~MARKDOWN
      ##### #{stats_heading} - Milestone #{plan.milestone.title}

      | Ratio | ~type::feature | ~type::bug | ~type::maintenance | Total | Calculation |
      | ----- | -------------- | ---------- | ------------------ | ----- | ----------- |
      | Target |  20% | 40% | 40% | 100% | manually set in planning |
      | Issue % |  #{Markdown.percentage(features.count,total)} | #{Markdown.percentage(bugs.count,total)} | #{Markdown.percentage(maintenance.count,total)} | 100% |  (issues of type) / (total issues) |
      | % of Closed Issues |  #{Markdown.percentage(closed_features.count,total_closed)} | #{Markdown.percentage(closed_bugs.count,total_closed)} | #{Markdown.percentage(closed_maintenance.count,total_closed)} | 100% | (closed of type) / (total closed) |
      | Say/Do | #{Markdown.percentage(closed_features.count,features.count)} | #{Markdown.percentage(closed_bugs.count,bugs.count)} | #{Markdown.percentage(closed_maintenance.count,maintenance.count)} | #{Markdown.percentage(total_closed,total)} | (closed of type) / (issues of type) |
      | # Issues | [#{features.count}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=all&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=type%3A%3Afeature&first_page_size=20) | [#{bugs.count}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=all&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=type%3A%3Abug&first_page_size=20) | [#{maintenance.count}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=all&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=type%3A%3Amaintenance&first_page_size=20) | [#{total}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&first_page_size=20) |  raw count |
      | # Closed Issues | [#{closed_features.count}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=closed&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=type%3A%3Afeature&first_page_size=20) | [#{closed_bugs.count}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=closed&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=type%3A%3Abug&first_page_size=20) | [#{closed_maintenance.count}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=closed&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=type%3A%3Amaintenance&first_page_size=20) | [#{total_closed}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&first_page_size=20) | raw count |
    
      ###### #{status_heading} - Milestone #{plan.milestone.title}
      
      | Workflow Stage | ~type::feature | ~type::bug | ~type::maintenance | Total # of issues |
      |----------------|--------------- | -------------- | ---------- | ------------------ |
      | ~"workflow::planning breakdown" | [#{workflow_count(features, "planning breakdown" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Aplanning%20breakdown&label_name%5B%5D=type%3A%3Afeature&first_page_size=20) | [#{workflow_count(bugs, "planning breakdown" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Aplanning%20breakdown&label_name%5B%5D=type%3A%3Abug&first_page_size=20) | [#{workflow_count(maintenance, "planning breakdown" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Aplanning%20breakdown&label_name%5B%5D=type%3A%3Amaintenance&first_page_size=20) | [#{workflow_count(issues, "planning breakdown" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Aplanning%20breakdown&first_page_size=20) |
      | ~workflow::blocked | [#{workflow_count(features, "blocked" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Ablocked&label_name%5B%5D=type%3A%3Afeature&first_page_size=20) | [#{workflow_count(bugs, "blocked" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Ablocked&label_name%5B%5D=type%3A%3Abug&first_page_size=20) | [#{workflow_count(maintenance, "blocked" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Ablocked&label_name%5B%5D=type%3A%3Amaintenance&first_page_size=20) | [#{workflow_count(issues, "blocked" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Ablocked&first_page_size=20) |
      | ~"workflow::ready for development" | [#{workflow_count(features, "ready for development" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Aready%20for%20development&label_name%5B%5D=type%3A%3Afeature&first_page_size=20) | [#{workflow_count(bugs, "ready for development" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Aready%20for%20development&label_name%5B%5D=type%3A%3Abug&first_page_size=20) | [#{workflow_count(maintenance, "ready for development" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Aready%20for%20development&label_name%5B%5D=type%3A%3Amaintenance&first_page_size=20) | [#{workflow_count(issues, "ready for development" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Aready%20for%20development&first_page_size=20) |
      | ~"workflow::in dev" | [#{workflow_count(features, "in dev" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Ain%20dev&label_name%5B%5D=type%3A%3Afeature&first_page_size=20) | [#{workflow_count(bugs, "in dev" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Ain%20dev&label_name%5B%5D=type%3A%3Abug&first_page_size=20) | [#{workflow_count(maintenance, "in dev" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Ain%20dev&label_name%5B%5D=type%3A%3Amaintenance&first_page_size=20) | [#{workflow_count(issues, "in dev" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Ain%20dev&first_page_size=20) |
      | ~"workflow::in review" | [#{workflow_count(features, "in review" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Ain%20review&label_name%5B%5D=type%3A%3Afeature&first_page_size=20) | [#{workflow_count(bugs, "in review" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Ain%20review&label_name%5B%5D=type%3A%3Abug&first_page_size=20) | [#{workflow_count(maintenance, "in review" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Ain%20review&label_name%5B%5D=type%3A%3Amaintenance&first_page_size=20) | [#{workflow_count(issues, "in review" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Ain%20review&first_page_size=20) |
      | ~"workflow::verification" | [#{workflow_count(features, "verification" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Averification&label_name%5B%5D=type%3A%3Afeature&first_page_size=20) | [#{workflow_count(bugs, "verification" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Averification&label_name%5B%5D=type%3A%3Abug&first_page_size=20) | [#{workflow_count(maintenance, "verification" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Averification&label_name%5B%5D=type%3A%3Amaintenance&first_page_size=20) | [#{workflow_count(issues, "verification" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Averification&first_page_size=20) |
      | ~"workflow::awaiting security release" | [#{workflow_count(features, "awaiting security release" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=type%3A%3Afeature&label_name%5B%5D=workflow%3A%3Aawaiting%20security%20release&first_page_size=20) | [#{workflow_count(bugs, "awaiting security release" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=type%3A%3Abug&label_name%5B%5D=workflow%3A%3Aawaiting%20security%20release&first_page_size=20) | [#{workflow_count(maintenance, "awaiting security release" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=type%3A%3Amaintenance&label_name%5B%5D=workflow%3A%3Aawaiting%20security%20release&first_page_size=20) | [#{workflow_count(issues, "awaiting security release" )}](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&milestone_title=#{plan.milestone.title}&label_name%5B%5D=#{CGI.escape(team.group_label)}#{deliverable_param}&label_name%5B%5D=workflow%3A%3Aawaiting%20security%20release&first_page_size=20) |

      **#{say_do_heading}: #{Markdown.percentage(total_closed,total)}**
    MARKDOWN
  end

  def team_activities_markdown
    activity = TeamActivity.new(weekly_update, weekly_update.monday, team)

    contributions_by_object_type = activity.contributions
      .group_by(&:object)
      .map { |object, events| "#{events.uniq(&:url).count} #{object}" }

    activity_by_user = activity.contributions.group_by(&:username).map do |username, contributions|
      by_type = contributions.group_by { Markdown.labelify(_1.type) }.sort_by { _2.count }.reverse
      by_group = contributions.group_by { Markdown.labelify(_1.group) }.sort_by { _2.count }.reverse
      by_category = contributions.group_by { Markdown.labelify(_1.category) }.sort_by { _2.count }.reverse
      by_object = contributions.group_by { Markdown.labelify(_1.object) }.sort_by { _2.count }.reverse

      for_team, for_others = contributions.partition { _1.group == team.group_label }
      count = contributions.count

      <<~MARKDOWN.chomp
        #### #{username} focus areas

        - Type: #{percentages_by_count(sort_by_count(by_type.to_h), count, truncate: true).join(', ')}
        - Group: #{percentages_by_count(sort_by_count(by_group.to_h), count, truncate: true).join(', ')}
        - Category: #{percentages_by_count(sort_by_count(by_category.to_h), count, truncate: true).join(', ')}
        - Work item: #{percentages_by_count(sort_by_count(by_object.to_h), count, truncate: true).join(', ')}

        <details><summary>~"#{team.group_label}" contributions</summary>

        #{format_contributions(for_team)}

        </details>
        <details><summary>other contributions</summary>

        #{format_contributions(for_others)}

        </details>
      MARKDOWN
    end

    <<~MARKDOWN.chomp
      <details><summary>Contributions on: #{Markdown.list(contributions_by_object_type)}</summary><br/>

        _Contributions span from #{activity.start_date} to #{activity.end_date}._

        #{activity_by_user.join("\n\n")}

        _Data from: Scraped from activity page for each user. Activities which don't support labels are
        mostly ignored._

        ---

      </details>
    MARKDOWN
  end

  def format_contributions(contributions)
    lines = []

    contributions.group_by(&:object).each do |object, object_events|
      lines << "#{object}"

      object_events.group_by(&:api_path).each do |path, path_events|
        event = path_events.first

        action_counts = path_events.group_by(&:action).map do |action, action_events|
          "#{action} (#{action_events.count})"
        end.join(', ')

        lines << "- #{contribution_label(event.url, event.details&.title)}: #{action_counts}"
      end
    end

    lines.join("\n")
  end

  def contribution_label(url, title, label = nil)
    if url&.include?('merge_requests')
      url = "#{url}.html"
      titl = title.match(/^(?<title>.{0,45})(\s|$)/)[:title]
      link = "[#{titl}](#{url})"
    else
      link = Markdown.gitlab_link(url)
    end

    "#{link} #{Markdown.labelify(label)}"
  end

  def issue_distribution_markdown
    type_dist = IssueDistribution.new(plan.milestone_issues)
    type_dist.group { _1.type ? Markdown.labelify(_1.type) : 'w/o type' }
    type_dist.sub_group { _1.sub_type ? Markdown.labelify(_1.sub_type) : 'w/o sub-type' }

    epic_dist = IssueDistribution.new(plan.milestone_issues)
    epic_dist.group { _1.epic ? "gitlab-org&#{_1.epic.iid}+" : 'w/o epic' }
    epic_dist.sub_group do
      if _1.engineering_domains.any?
        _1.engineering_domains.map { |domain| Markdown.labelify(domain) }.join(' ')
      else
        'w/o eng label'
      end
    end

    <<~MARKDOWN.chomp
      <details><summary>#{plan.milestone.title} focus area distribution</summary>

        #{issue_distribution_table(type_dist)}

        #{issue_distribution_table(epic_dist)}

      </details>
    MARKDOWN
  end

  def issue_distribution_table(distribution)
    header = percentages_by_count(
      distribution.grouped,
      distribution.group_count,
      default_label: distribution.label
    )

    cells = distribution.sub_grouped.map do |sub_distribution|
      subsections = percentages_by_count(
        sub_distribution.grouped,
        sub_distribution.group_count,
        default_label: sub_distribution.label
      )

      Markdown.multiline_cell(subsections)
    end

    Markdown.table(header, [cells])
  end

  def progress_summary_markdown
    feature_completion = plan.by_epic.map do |feature, issues|
      if epic = issues.first.epic
        dashboard_link = "https://gitlab.com/groups/gitlab-org/-/issues/?milestone_title=#{plan.milestone.title}&not%5Blabel_name%5D%5B%5D=Planning%20Issue&epic_id=#{epic&.id}"
        epic_string = "from epic: gitlab-org&#{epic&.iid}+"
      else
        dashboard_link = "https://gitlab.com/groups/gitlab-org/-/issues/?milestone_title=#{plan.milestone.title}&not%5Blabel_name%5D%5B%5D=Planning%20Issue&epic_id=None"
        epic_string = "without an epic"
      end

      "#{percentage_by_weight(issues)} - of [scheduled issues](#{dashboard_link}) #{epic_string}"
    end

    <<~MARKDOWN.chomp
      <h4>~#{percentage_by_weight(plan.milestone_issues)} complete of #{plan.milestone.title} scheduled work</h4>

      #{feature_completion.map { '- ' + _1  }.join("\n")}

      _Percentage is a rough estimate and could be quite wrong. The calculation codifies `@syasonik`'s mental model for ~% completion, based on workflow labels, open MRs, and domain labels._

    MARKDOWN
  end

  def progress_detail_markdown
    tables = plan.by_epic.map do |feature, issues|
      header = "##### #{feature || 'Standalone issues'}  -  #{percentage_by_weight(issues)} complete"

      detailed_progress_table(header, issues)
    end

    tables.join('')
  end

  def detailed_progress_table(header, issues)
    rows = issues.map do |issue|
      title = "#{issue.web_url}+"
      key_labels = Markdown.label_list(issue.notable_labels)
      type = "Type: #{Markdown.label_list([issue.type, issue.sub_type])}"
      domains = "Domains: #{Markdown.label_list(issue.domains)}"
      assignees = Markdown.user_list(issue.assignees.map(&:username))
      ownership = Markdown.multiline_cell([assignees, issue.missing_assignee, "", issue.status])
      review_status = Markdown.multiline_cell(issue.review_status(weekly_update))
      percent_completion = "**#{Markdown.percentage(issue.completion_ratio)}**"

      if issue.errors.any?
        warnings = Markdown.multiline_cell(issue.errors.to_a, bulleted: true)
        warning_markdown = "<details><summary>Possible action items required</summary>#{warnings}</details>"
      end

      [
        Markdown.multiline_cell([title, key_labels, "", type, domains, warning_markdown]),
        Markdown.multiline_cell([ownership, review_status], divider: true),
        percent_completion
      ]
    end

    <<~MARKDOWN
      #{header}

      #{Markdown.table(['Issue', 'Status', "~% Complete"], rows)}

    MARKDOWN
  end

  def rollover_issues_markdown
    issues_by_milestone = plan.rollover_issues.group_by { _1.milestone.title }
    rollover_milestones = issues_by_milestone.keys.map { "%\"#{_1}\""}

    rollover_sections = issues_by_milestone.map do |milestone, issues|
      header = "**%\"#{milestone}\" rollover:**"
      list_items = issues.map { |issue| "- #{issue.web_url}+ `#{issue.assignee.username}`" }

      "#{header}\n#{list_items.join("\n")}"
    end

    <<~MARKDOWN.chomp
      <details><summary>#{plan.rollover_issues.count} Rollover issues from #{rollover_milestones.join(', ')}</summary><br/>

      #{rollover_sections.join("\n\n")}


      _Data from: GitLab REST API. Contains open issues which are
      in a prior milestone & assigned to a ~"#{team.group_label}" engineer._

      ---

      </details>
    MARKDOWN
  end

  def team_links_markdown
    <<~MARKDOWN
      - [Current Planning Issue](#{plan.current_planning_issue&.web_url})
      - [Milestone Issues](https://gitlab.com/groups/gitlab-org/-/boards/1372896?milestone_title=#{plan.milestone.title})
    MARKDOWN
  end

  def quick_actions_markdown
    <<~MARKDOWN
      /milestone %"#{plan.milestone.title}"

    MARKDOWN
  end

  def highlights_markdown
    issue = weekly_update.current
    return unless issue&.user_notes_count.to_i > 0

    threads = JankyCache.fetch("/projects/#{issue.project_id}/issues/#{issue.iid}/discussions") { weekly_update.gitlab_api.get(_1) }
    thread = threads.find { _1.notes.first.body.match?(/highlights/i) }
    return unless thread

    highlights = thread.notes[1..-1].map do |note|
      lines = note.body.split("\n")
      next note.body if lines.all? { _1.match?(/^\s*-\s+\b/) }

      lines.filter_map { '- ' + _1 unless _1.empty? }.join("\n")
    end

    highlights.join("\n")
  end

  def sort_by_count(grouped_issues)
    grouped_issues
      .sort_by { _2.count }
      .reverse
      .to_h
  end

  # @param issues_by_category Hash<String/Symbol, Array<Issue>>
  # @param total Integer
  def percentages_by_count(issues_by_category, total, truncate: false, default_label: 'unlabeled')
    if truncate
      big, small = issues_by_category.partition { |category, issues| issues.count >= 0.02 * total && category }
      other_issues = small.to_h.values.flatten

      if other_issues.any?
        default_label = 'other (<3% each)'
        issues_by_category = big.to_h.merge(nil => other_issues)
      end
    end

    issues_by_category.map do |category, issues|
      percent = Markdown.percentage(issues.count, total)

      "#{percent} #{category || default_label}"
    end
  end

  # @param issues Array<Issue>
  def percentage_by_weight(issues)
    numerator = issues.sum { |issue| issue.weight * issue.completion_ratio }
    denominator = issues.sum(&:weight)

    Markdown.percentage(numerator, denominator)
  end

  def okr_table_markdown

    # TODO: Retrieve the OKR issues to generate the table

    fy = fiscal_year(plan.today)
    q = quarter(plan.today)

    <<~MARKDOWN.chomp

      OKRS for #{fy}#{q} [GitLab](https://gitlab.com/gitlab-com/gitlab-OKRs/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=#{CGI.escape(team.group_label)}&label_name%5B%5D=#{fy}-#{q}&first_page_size=20).

      | Item | Sub-item | Progress | Change Direction |
      | ------ | ------ | ------ | ------ |
      | | | | |

    MARKDOWN

  end

  def mr_breakdown_markdown

    mrs = plan.team_merge_requests
    feature_mr_count = mrs.select{ |mr| mr.feature? }.count
    bug_mr_count = mrs.select{ |mr| mr.bug? }.count
    maintenance_mr_count = mrs.select{ |mr| mr.maintenance? }.count
    merged_mrs = mrs.select{ |mr| mr.merged? }
    merged_mrs_feature_mr_count = merged_mrs.select{ |mr| mr.feature? }.count
    merged_bug_mr_count = merged_mrs.select{ |mr| mr.bug? }.count
    merged_maintenance_mr_count = merged_mrs.select{ |mr| mr.maintenance? }.count

      <<~MARKDOWN.chomp

      | Ratio | ~type::feature | ~type::bug | ~type::maintenance | Total | Calculation |
      | ----- | -------------- | ---------- | ------------------ | ----- | ----------- |
      | MRs in Milestone | #{Markdown.percentage(feature_mr_count,mrs.count)} | 28.5% | 43% | 100% | (MRs of type) / (total MRs) |
      | # MRs | #{feature_mr_count} | #{bug_mr_count} | #{maintenance_mr_count} | #{mrs.count}| raw count |
      | # Merged MRS |  #{merged_mrs_feature_mr_count} | #{merged_bug_mr_count} | #{merged_maintenance_mr_count} | #{merged_mrs.count} | raw count |

    MARKDOWN
  end

  def community_contributions_markdown
    #TODO: load the data
    #TODO: load in the reviews data?

    # <!-- REPLACE-WITH-COMMUNITY-CONTRIBUTIONS:CREATE -->
    <<~MARKDOWN.chomp

      ##### ~"Community contribution"

      ###### Workflow Status of Open Issues

      | Workflow Stage | Total # of issues |
      |----------------| ----------------- |
      | ~"workflow::in review" | 1 |

      Completed this milestone: 0
    MARKDOWN
  end

  private

  def workflow_count(issues, status)
    issues.select { |i| i.status == "~\"workflow::#{status}\"" }.count
  end
end
