# Represents team-specific configuration for generating the weekly update
class Team
  attr_reader :config

  def initialize(config)
    @config = config
  end

  def all
    @all ||= config['engineers'].values.flatten.uniq
  end

  def frontend
    @frontend ||= config.dig('engineers', 'frontend')
  end

  def backend
    @backend ||= config.dig('engineers', 'backend')
  end

  def fullstack
    @fullstack ||= config.dig('engineers', 'fullstack') || []
  end

  def manager
    @manager ||= config.dig('engineers', 'manager')
  end

  def frontend_borrow
    config.dig('borrow', 'frontend') || 0
  end

  def backend_borrow
    config.dig('borrow', 'backend') || 0
  end

  def display_name
    config['group_name']
  end

  def group_name_snake
    config['group_name'].downcase.tr(' ', '_')
  end

  def group_name_kabob
    config['group_name'].downcase.tr(' ', '-')
  end

  def group_label
    config['group_label']
  end

  def help_group
    config['help_group_label']
  end

  def planning_issue_project
    config['planning_issue_project'] || []
  end

  def availability_url
    config['error_budget_availability']
  end

  def capacity_url
    config['capacity_url']
  end

  def team_calendar
    config['team_calendar']
  end

  def update_template
    config['issue_template']
  end

  def update_project_id
    config['update_project_id']
  end
end