# frozen_string_literal: true

class OkrPlan
  attr_reader :gitlab_api, :today

  def initialize(gitlab_api, today)
    @gitlab_api = gitlab_api
    @today = today
  end

end
