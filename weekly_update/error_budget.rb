# Queries public snapshot of Grafana chart for team availability

require './weekly_update/error_budget/error_budget_data'

class ErrorBudget

  def initialize(date, team)
    gcp_keyfile = ENV["PERIODIC_QUERY_GCP_KEYFILE_PATH"]
    gcp_project = ENV["PERIODIC_QUERY_GCP_PROJECT"]
    gcs_bucket = ENV["PERIODIC_QUERY_BUCKET"]

    @metrics = load_error_budget_data(gcp_project, gcs_bucket, gcp_keyfile, team.group_name_snake, date, seven_day=false)
    @metrics_7d = load_error_budget_data(gcp_project, gcs_bucket, gcp_keyfile, team.group_name_snake, date, seven_day=true)
  end

  def availability_28d
    @metrics.availability.to_f
  end

  def minutes_remaining_28d
    @metrics.seconds_remaining.to_f.fdiv(60).round(1)
  end

  def minutes_spent_28d
    @metrics.seconds_spent.to_f.fdiv(60).round(1)
  end


  def availability_7d
    @metrics_7d.availability.to_f
  end

  def minutes_remaining_7d
    @metrics_7d.seconds_remaining.to_f.fdiv(60).round(1)
  end

  def minutes_spent_7d
    @metrics_7d.seconds_spent.to_f.fdiv(60).round(1)
  end

  def status_28d
    return "⚠️ " unless availability_28d&.is_a?(Float)
    return "🟢" if availability_28d >= 0.9997
    return "🟡" if availability_28d >= 0.9995
    "🔴"
  end

  def status_7d
    return "⚠️ " unless availability_7d&.is_a?(Float)
    return "🟢" if availability_7d >= 0.9997
    return "🟡" if availability_7d >= 0.9995
    "🔴"
  end
end

