
module FiscalCalendar

  # Return the quarter based on the year starting in February.
  def quarter(date)
    if date.mon >= 2 && date.mon < 5
      'Q1'
    elsif date.mon >= 5 && date.mon < 8
      'Q2'
    elsif date.mon >= 8 && date.mon < 11
      'Q3'
    else
      'Q4'
    end
  end

  # Return the fiscal year based on starting in February. Ex. FY24 starts Feb 2023
  def fiscal_year(date)
    fy = date.strftime("%y").to_i
    if date.mon >= 2
      fy = fy + 1
    end

    "FY#{fy}"
  end

end