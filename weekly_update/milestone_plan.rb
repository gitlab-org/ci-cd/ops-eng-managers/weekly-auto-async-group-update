# Represents the team's planning issue for the current milestone
class MilestonePlan
  attr_reader :gitlab_api, :today, :team, :weekly_update

  def initialize(gitlab_api, today, team, weekly_update)
    @gitlab_api = gitlab_api
    @today = today
    @team = team
    @weekly_update = weekly_update
  end

  # Group by epic
  def by_epic
    @by_epic ||= milestone_issues.group_by(&:epic_title)
  end

  def milestone
    @milestone ||= JankyCache.fetch("milestone-#{today}") do
      gitlab_api
        .group_milestones("gitlab-org", state: :active, per_page: 100)
        .find do |details|
          details.due_date &&
          details.start_date &&
          today <= Date.parse(details.due_date) &&
          today >= Date.parse(details.start_date)
        end
      end
  end

  def milestone_issues
    @milestone_issues ||= []
    if @milestone_issues.empty?
      gitlab_api
        .group_issues(
          "gitlab-org",
          milestone: milestone.title,
          labels: [team.group_label],
          not: { labels: ['Planning Issue', 'OpsSection::Weekly-Update', 'type:ignore'] }).each_page do |page|
        @milestone_issues += page.map do |details|
          issue = Issue.new(details, team).tap { _1.merge_requests = fetch_related_mrs(_1) }
        end
      end
    end
    @milestone_issues
  end

  def rollover_issues
    @rollover_issues ||= team.all.flat_map do |username|
      JankyCache.fetch("rollover-#{milestone.id}-#{username}") do
        gitlab_api.issues(
          nil, {
          not: { milestone: milestone.title, labels: ['Planning Issue', 'OpsSection::Weekly-Update', 'type:ignore'] },
          state: :opened,
          assignee_username: username,
          milestone_id: :Started,
          scope: :all,
        })
      end
    end
  end

  def security_issues
    @security_issues ||= gitlab_api
                            .group_issues(
                              "gitlab-org",
                              labels: [team.group_label, 'bug::vulnerability', 'security'],
                              state: 'opened')
                            .map do |details|
      issue = Issue.new(details, team).tap { _1.merge_requests = fetch_related_mrs(_1) }
    end
  end

  def infradev_issues
    @infradev_issues ||= gitlab_api
                           .group_issues(
                             "gitlab-org",
                             labels: [team.group_label, 'infradev'],
                             state: 'opened')
                           .map do |details|
      issue = Issue.new(details, team).tap { _1.merge_requests = fetch_related_mrs(_1) }
    end
  end

  def corrective_action_issues
    @corrective_action_issues ||= gitlab_api
                           .group_issues(
                             "gitlab-org",
                             labels: [team.group_label, 'corrective action'],
                             state: 'opened')
                           .map do |details|
      issue = Issue.new(details, team).tap { _1.merge_requests = fetch_related_mrs(_1) }
    end
  end

  def help_issues
    @help_issues ||= gitlab_api
                           .group_issues(
                             "gitlab-com",
                             labels: [team.help_group],
                             state: 'opened')
                           .map do |details|
      issue = Issue.new(details, team).tap { _1.merge_requests = fetch_related_mrs(_1) }
    end
  end

  def team_merge_requests
    @merge_requests ||= gitlab_api
                        .group_merge_requests(
                          "gitlab-org",
                          labels: [team.group_label],
                          milestone: milestone.title)
                        .map do |details|
      mr = MergeRequest.new(details)
    end
  end

  def current_planning_issue
    planning_issues = gitlab_api.issues(
      team.planning_issue_project,
      labels: ["Planning Issue", team.group_label],
      search: milestone.title,
      in: "title",
      )
    planning_issues.first
  end

  private

  # This info isn't batch-able, so we have to call the endpoint for each issue :(
  def fetch_related_mrs(issue)
    puts "Fetching related MRs for issue ##{issue.id}..."

    def date_filter(mrs)
      mrs.select { |mr| Date.strptime(mr.updated_at) >= weekly_update.monday }
    end

    JankyCache.fetch("/projects/#{issue.project_id}/issues/#{issue.iid}/related_merge_requests") { date_filter(gitlab_api.get(_1)) }
  end


end