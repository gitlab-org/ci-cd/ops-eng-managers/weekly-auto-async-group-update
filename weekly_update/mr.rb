class MergeRequest < SimpleDelegator

  TYPE_LABELS = ["type::", "bug::", "feature::", "maintenance::", "workflow::"]
  DOMAIN_LABELS = ["backend", "frontend", "documentation", "UX", "database", "Quality"]
  NOTABLE_LABELS = ["severity::", "backend complete", "bug::vulnerability", "deprecation", "removal", "Community contribution", "SLO::", "Deliverable"]

  def type
    @type ||= relevant_labels.find { |label| label.include?("type::") }
  end

  def merged?
    state == 'merged'
  end

  def feature?
    type == "type::feature"
  end

  def bug?
    type == "type::bug"
  end

  def maintenance?
    type == "type::maintenance"
  end

  private

  def relevant_labels
    @relevant_labels ||= labels.select do |label|
      DOMAIN_LABELS.any? { |full_label| label == full_label } ||
        (TYPE_LABELS + NOTABLE_LABELS).any? { |prefix| label.include?(prefix) }
    end
  end
end